# Email Case Details Function

## Overview

An org configuration that allows users to generate a pdf with case details and send it to the contacts associated with the case's account in two different ways. (1) Utilizing the process builder and a checkbox when creating/editing a case. (2) By using a custom button on the case detail page.

## Demo (~2 min)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=Ze8QBudhjwk)

## Some Screenshots

### Generated PDF Example

![Generated PDF Example](media/sample-pdf.png)

### Custom Button Modal

![Custom Button Modal](media/button-modal.png)

### Process Builder on Case

![Process Builder on Case](media/PB-case.png)

## File Overview

### Apex

* **CaseEmailService** - Handles sending case detail emails to appropriate contacts. Utilized by both process builder and the detail-page modal component.
* **EmailUtils** - Helper for CaseEmailService. Generates PDF attachment from VF page and prepares subject/body of email.
* **CaseDetailsController** - Controller for detail-page modal component.
* **CaseDetailsEmailQueueable** - Queueable that calls on CaseEmailService to send an email when it executes. Required for the process builder to work because we cannot generate the details PDF in the same transaction that we save the case.


### Aura

* **CaseDetailsEmailAction** - Modal component for sending case details to selected contacts associated with the case's account.

### Visualforce

* **CaseDetailsPage** - Visualforce page that renders as a pdf and is attached to the emails. See screenshots section for an example.