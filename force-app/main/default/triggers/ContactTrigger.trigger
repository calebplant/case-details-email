trigger ContactTrigger on Contact (after delete, after undelete) {
    if(Trigger.isAfter) {
        if(Trigger.isDelete) {
            System.debug('START after delete');
            ContactTrigger_Helper.deleteOperation(Trigger.old);
        } else if (Trigger.isUndelete) {
            System.debug('START after undelete');
            ContactTrigger_Helper.deleteOperation(Trigger.new);
        }
    }
}