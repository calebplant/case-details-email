public with sharing class ContactTrigger_Helper {
    
    public static void deleteOperation(List<Contact> cons)
    {
        Set<Id> accIds = new Set<Id>();
        for(Contact eachCon : cons)
        {
            accIds.add(eachCon.AccountId);
        }

        List<Account> accsToUpdate = [SELECT Id, Contacts_With_Emails__c, (SELECT Id FROM Contacts WHERE Email != null)
                                      FROM Account
                                      WHERE Id IN :accIds];
        for(Account eachAcc : accsToUpdate) {
            eachAcc.Contacts_With_Emails__c = eachAcc.Contacts.size();
        }
        update accsToUpdate;
    }
}
