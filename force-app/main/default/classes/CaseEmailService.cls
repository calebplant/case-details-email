public with sharing class CaseEmailService {

    @InvocableMethod(label='Email Case Details' description='Send case details email to contacts associate with case\'s account' category= 'Case')
    public static void sendDetailsFromProcessBuilder(List<CaseEmailRequest> cases) {
        System.debug('START sendDetailsFromProcessBuilder');
        Case targetCase = cases[0].targetCase;
        List<Contact> recipients = findRecipients(targetCase);
        // Have to use queueable because Case was inserted in this context, and VF cannot render a pdf with its details yet
        System.debug('Enqueue email job');
        Id jobId = System.enqueueJob(new CaseDetailsEmailQueueable(targetCase, recipients));
        System.debug('Job queued: ' + jobId);
        // sendDetailsEmail(targetCase, recipients);
    }
    
    public static void sendDetailsEmail(Case targetCase, List<Contact> recipients)
    {
        System.debug('START sendDetails');
        if(!(recipients.size() > 0)) {
            return;
        }
        // System.debug('targetCase: ' + targetCase);
        // System.debug('recipients: ' + recipients);
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        System.debug('Generating pdf');
        Blob attachmentBody = EmailUtils.getCaseDetailsAsPdf(targetCase.Id);
        System.debug('Attaching');
        attachment.setBody(attachmentBody);
        attachment.setContentType('application/pdf');
        attachment.setFileName('case-details.pdf');
        attachment.setInline(false);
        List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
        emailAttachments.add(attachment);
        
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        for(Contact eachRecipient : recipients) {
            System.debug('Generating email');
            Messaging.SingleEmailMessage outboundEmail = new Messaging.SingleEmailMessage();
            outboundEmail.setTargetObjectId(eachRecipient.Id);
            outboundEmail.setTreatTargetObjectAsRecipient(true);
            outboundEmail.setSaveAsActivity(false);
            outboundEmail.setUseSignature(true); 
            outboundEmail.setSubject(EmailUtils.buildCaseDetailsSubject(targetCase.CaseNumber));
            outboundEmail.setHtmlBody(EmailUtils.buildCaseDetailsBody(eachRecipient.Name, targetCase.CaseNumber, targetCase.Subject));
            outboundEmail.setFileAttachments(emailAttachments);
            outboundEmails.add(outboundEmail);
        }

        System.debug('Sending Email: ' + outboundEmails);
        Messaging.sendEmail(outboundEmails);
        System.debug('Email sent!');
    }

    private static List<Contact> findRecipients(Case targetCase)
    {
        // Id accountId = [SELECT Id FROM Account WHERE Id = :targetCase.AccountId].Id;
        return [SELECT Id, Name, Email FROM Contact
                WHERE AccountId = :targetCase.AccountId
                AND Email != null];
    }

    public class CaseEmailRequest {
        @InvocableVariable(required=true)
        public Case targetCase;
        @InvocableVariable(required=true)
        public Boolean isCreate;
    }
}
