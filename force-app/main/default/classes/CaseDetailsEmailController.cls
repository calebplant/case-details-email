public with sharing class CaseDetailsEmailController {
    
    @AuraEnabled
    public static List<Contact> getRelatedContacts(Id accountId)
    {
        System.debug('Passed: ' + accountId);
        return [SELECT Id, Name, Email FROM Contact
                WHERE AccountId = :accountId
                AND Email != null];
    }

    @AuraEnabled
    public static void sendCaseDetailsToContact(Case targetCase, List<Id> conIds)
    {
        System.debug('START sendCaseDetailsToContact');
        System.debug('conIds: ' + conIds);
        List<Contact> cons = [SELECT Id, Name FROM Contact WHERE Id IN :conIds];
        System.debug('cons: ' + cons);
        CaseEmailService.sendDetailsEmail(targetCase, cons);
    }
}
