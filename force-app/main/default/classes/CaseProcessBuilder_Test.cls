@isTest
public with sharing class CaseProcessBuilder_Test {
    
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='Banana Republic');
        insert acc;

        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName='One', AccountId=acc.Id, Email='One@gmail.com'));
        cons.add(new Contact(LastName='Two', AccountId=acc.Id, Email='Two@gmail.com'));
        cons.add(new Contact(LastName='Three', AccountId=acc.Id));
        insert cons;
    }

    @isTest
    static void doesCreateSendCreateDetailsEmail()
    {
        Account acc = [SELECT Id FROM Account];

        Case newCase = new Case();
        newCase.AccountId = acc.Id;
        newCase.Email_Case_Details__c = True;

        Test.startTest();
        insert newCase;
        Test.stopTest();
    }
}
