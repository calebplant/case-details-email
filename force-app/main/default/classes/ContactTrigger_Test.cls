@isTest
public with sharing class ContactTrigger_Test {
    
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='Banana Republic');
        insert acc;

        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName='One', AccountId=acc.Id, Email='One@gmail.com'));
        cons.add(new Contact(LastName='Two', AccountId=acc.Id, Email='Two@gmail.com'));
        cons.add(new Contact(LastName='Three', AccountId=acc.Id));
        insert cons;
    }

    @isTest
    static void doesContactDeleteUpdateRelatedAccount()
    {
        Contact con1 = [SELECT Id FROM Contact WHERE Name = 'One'];
        Account acc = [SELECT Id, Contacts_With_Emails__c FROM Account];
        Decimal startValue = acc.Contacts_With_Emails__c;

        Test.startTest();
        delete con1;
        Test.stopTest();

        acc = [SELECT Id, Contacts_With_Emails__c FROM Account];
        System.assertEquals(startValue - 1, acc.Contacts_With_Emails__c);
    }

    @isTest
    static void doesUndeleteContactUpdateRelatedAccount()
    {
        Contact con1 = [SELECT Id FROM Contact WHERE Name = 'One'];
        delete con1;
        Account acc = [SELECT Id, Contacts_With_Emails__c FROM Account];
        Decimal startValue = acc.Contacts_With_Emails__c;

        Test.startTest();
        undelete con1;
        Test.stopTest();

        acc = [SELECT Id, Contacts_With_Emails__c FROM Account];
        System.assertEquals(startValue + 1, acc.Contacts_With_Emails__c);
    }
}
