public with sharing class CaseDetailsEmailQueueable implements Queueable, Database.AllowsCallouts {

    private Case targetCase;
    private List<Contact> recipients;
    
    public CaseDetailsEmailQueueable(Case targetCase, List<Contact> recipients)
    {
        this.targetCase = targetCase;
        this.recipients = recipients;
    }

    public void execute(QueueableContext context)
    {
        System.debug('START CaseDetailsEmailQueueable');
        CaseEmailService.sendDetailsEmail(targetCase, recipients);
    }
}
