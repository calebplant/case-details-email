public with sharing class EmailUtils {

    public static Blob getCaseDetailsAsPdf(Id caseId)
    {
        try {
            System.debug('get page reference');
            System.debug('caseId:' + caseId);
            PageReference detailsPdf = Page.CaseDetailsPage;
            detailsPdf.getParameters().put('id', caseId);
    
            if(Test.isRunningTest()) { // Cannot run getContentAsPDF() during test
                return Blob.valueOf('Unit.Test'); 
            }
            System.debug('render as pdf');
            Blob result = detailsPdf.getContent();
            return result;
        } catch (VisualforceException e) {
            System.debug('Error');
            System.debug(e);
            System.debug(e.getMessage());
            System.debug(UserInfo.getName());
            System.debug(UserInfo.getProfileId());
        }
        return null;
    }

    public static String buildCaseDetailsSubject(String caseNumber)
    {
        return 'Case Details: ' + caseNumber;
    }

    public static String buildCaseDetailsBody(String recipientName, String caseNumber, String caseSubject)
    {
        String body = 'Dear ' + recipientName + ',<br><br>' +
                'Please find the case details attached to this email.<br><br>' +
                '<b>Case Number:</b> ' + caseNumber + '<br>' +
                '<b>Case Subject:</b> ' + caseSubject + '<br><br>';
        return body;
    }
}
