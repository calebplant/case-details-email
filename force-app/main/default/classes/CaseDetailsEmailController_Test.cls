@isTest
public with sharing class CaseDetailsEmailController_Test {

    private static Integer numOfStartCons;
    private static Integer consWithEmailsAssociatedWithAcc = 2;

    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='Banana Republic');
        insert acc;


        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName='One', AccountId=acc.Id, Email='One@gmail.com'));
        cons.add(new Contact(LastName='Two', AccountId=acc.Id, Email='Two@gmail.com'));
        cons.add(new Contact(LastName='Three', AccountId=acc.Id));
        insert cons;

        List<Case> cases = new List<Case>();
        cases.add(new Case(AccountId=acc.Id, Subject='Blabla'));
        insert cases;

        numOfStartCons = cons.size();
    }
    
    @isTest
    static void doesGetRelatedContactsGetRelatedContactsForAccount()
    {
        Account acc = [SELECT Id, Contacts_With_Emails__c FROM Account];

        Test.startTest();
        List<Contact> foundCons = CaseDetailsEmailController.getRelatedContacts(acc.Id);
        Test.stopTest();

        System.assertEquals(consWithEmailsAssociatedWithAcc, foundCons.size());
    }

    @isTest
    static void doesSendCaseDetailsToContactSendAnEmail()
    {
        
        Contact con1 = [SELECT Id FROM Contact WHERE Name = 'One'];
        Case targetCase = [SELECT Id, Subject, CaseNumber  FROM Case WHERE Subject='Blabla' LIMIT 1];

        Test.startTest();
        Integer startEmailLimit = Limits.getEmailInvocations();
        CaseDetailsEmailController.sendCaseDetailsToContact(targetCase, con1.Id);
        Integer stopEmailLimit = Limits.getEmailInvocations();
        Test.stopTest();

        System.assert(startEmailLimit < stopEmailLimit);
    }
}
