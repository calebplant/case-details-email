({
    init : function(component, event, helper) {
        console.log('init');
    },

    handleOpenPreview : function(component, event, helper) {
        console.log('handleOpenPreview');
        var recordId = component.get('v.recordId');
        console.log('recordID: ' + recordId);
        window.open('/apex/CaseDetailsPage?id=' + recordId);
    },

    handleSend : function(component, event, helper) {
        console.log('handleSend');

        var action = component.get('c.sendCaseDetailsToContact');
        let targetCase = component.get('v.caseRecord');
        let cons = component.get('v.selectedContacts');
        console.log('cons:');
        console.log(cons);
        let toastTitle = '';
        let toastType = '';
        let toastMessage = '';

        action.setParams({targetCase: targetCase, conIds: cons});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('From server: ');
                console.log(response.getReturnValue());

                toastTitle = 'Email Sent';
                toastType = 'success';
                toastMessage = 'Successfully sent email!';
            }
            else if (state === 'INCOMPLETE') {
                console.log('incomplete');
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + 
                                    errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
                toastTitle = 'Error Sending Email';
                toastType = 'error';
                toastMessage = 'Error calling server side action!\n' + errors;
            }
            console.log('close modal');
            // Close the action panel
            var dismissActionPanel = $A.get('e.force:closeQuickAction');
            dismissActionPanel.fire();
            // Show toast
            console.log('show toast');
            var toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
                    'title': toastTitle,
                    'type': toastType,
                    'message': toastMessage
                });
            toastEvent.fire();

            });

        $A.enqueueAction(action);
    },

    handleCancel : function(component, event, helper) {
        console.log('handleCancel');

        // Close the action panel
        var dismissActionPanel = $A.get('e.force:closeQuickAction');
        dismissActionPanel.fire();
    },

    handleRecordLoaded : function(component, event, helper) {
        console.log('handleRecordLoaded');

        var eventParams = event.getParams();
        if(eventParams.changeType === 'LOADED') {
            // record is loaded (render other component which needs record data value)
             console.log('Record loaded successfully.');
             console.log('Loaded account Id: ' + component.get('v.caseRecord.AccountId'));

             var action = component.get('c.getRelatedContacts');
             let accountId = component.get('v.caseRecord.AccountId');
             action.setParams({accountId: accountId});
             action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    console.log('From server: ');
                    console.log(response.getReturnValue());

                    let cons = [];
                    let res = response.getReturnValue();
                    res.forEach(eachCon => {
                        console.log('eachCon:');
                        console.log(eachCon);
                        cons.push({
                            'label': eachCon.Name,
                            'value': eachCon.Id
                        });
                    })
                    component.set('v.contactOptions', cons);
                    let conValues = res.map(eachCon => {
                        return eachCon.Id;
                    });
                    console.log('conValues');
                    console.log(conValues);
                    component.set("v.startingSelectedContacts", conValues);
                    component.set("v.selectedContacts", conValues);
                }
                else if (state === 'INCOMPLETE') {
                    console.log('incomplete');
                }
                else if (state === 'ERROR') {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log('Error message: ' + 
                                     errors[0].message);
                        }
                    } else {
                        console.log('Unknown error');
                    }
                }
            });
            $A.enqueueAction(action);

         } else if(eventParams.changeType === 'CHANGED') {
             console.log('CHANGED');
         } else if(eventParams.changeType === 'REMOVED') {
             console.log('REMOVED');
            } else if(eventParams.changeType === 'ERROR') {
             console.log('Error: ' + component.get('v.recordError'));
         }
    },

    handleContactChange : function(component, event, helper) {
        var selectedOptionValue = event.getParam('value');
        let checkbox =  event.getSource();
        let sendButton = component.find("sendButtonId");
        console.log(checkbox.get('v.validity').valid);

        if(event.getSource().get('v.validity').valid) { 
            sendButton.set('v.disabled', false);
        } else { // No options selected
            console.log('disable it');
            sendButton.set('v.disabled', true);
        }
        component.set('v.selectedContacts', selectedOptionValue);
    },

    showSentToast : function(component, event, helper) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            'title': 'Success!',
            'message': 'The record has been updated successfully.'
        });
        toastEvent.fire();
    },

    showSentErrorToast : function(component, event, helper) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            'title': 'Success!',
            'message': 'The record has been updated successfully.'
        });
        toastEvent.fire();
    }

})
